'use strict';

const http = require('http');
const express = require('express');
const fs = require('fs');
const path = require('path');
let io = require('socket.io');
// const crypto = require('crypto');
const colors = require('colors/safe');

const app = express();
const staticDir = express.static;
const server = http.createServer(app);

const serverConfig = require('../multiplex-server.config.json');

const DEFAULT_PORT = 1948;
const LOCALHOST = '127.0.0.1';
let serverURL = serverConfig.serverURL.replace('http://', '');
let address = serverURL.split(':')[0];
address = address.length < 1 ? LOCALHOST : address;

let port = Number(serverURL.split(':')[1]);
port = isNaN(port) ? DEFAULT_PORT : port;

io = io(server);

const opts = {
    port: port,
    baseDir: path.resolve(__dirname, serverConfig.baseDir) //Assuming this server will be in dist_client
};

io.on('connection', function (socket) {
    //console.log(colors.yellow("Connected"), socket.handshake.address);
    socket.on('multiplex-statechanged', function (data) {
       // console.log(colors.yellow("multiplex-statechanged"), data, data.secret === serverConfig.socketSecret);

        if (typeof data.secret == 'undefined' || data.secret == null || data.secret === '') return;
        if (data.secret === serverConfig.socketSecret) {
            data.secret = null;
            io.emit(data.socketId, data);
           // console.log(colors.green("Emitted multiplex-statechanged"), data.socketId, data)
        };
    });
});

//console.log(colors.yellow("BASE DIR %s"), opts.baseDir);
app.use(express.static(opts.baseDir));

app.get("/", function (req, res) {
    res.redirect('/index.html');
});

//Listen for connections
server.listen(opts.port || null, address);
console.log(colors.rainbow('reveal.js'), colors.white(" Multiplex running on port "), colors.green(`${address}:${port}`));