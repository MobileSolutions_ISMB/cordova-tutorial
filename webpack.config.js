'use strict';

const fs = require('fs');
const webpack = require('webpack');
const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const autoprefixer = require('autoprefixer');
const marked = require('marked');

let hljs = require('highlight.js');

//Some fine tuning of bash highlight
let bash = hljs.getLanguage('bash');
bash.keywords.built_in = bash.keywords.built_in + ' mkdir git rm';

let HL_CMD_BUILT_IN = {
    className: 'built_in',
    begin: /^(webpack|cordova|npm|node)(?!-|\.|_)/,
    end: /(\b|$)/
};

bash.contains.push(HL_CMD_BUILT_IN);

//options
bash.contains.push({
    className: 'string',
    begin: /\s(--([\w-])*|-(\w)+)/,
    end: /(\b|$)/
})

//commands
bash.contains.push({
    className: 'section',
    begin: /[^-\.](init|install|clone|add|remove|prepare|build|run|serve|clean|compile|help|create|info|requirements|platform|plugin|emulate|run-script)\b(?!-|\.|_)/,
    end: /(\b|$)/
});

marked.setOptions({
    highlight: function (code) {
        return hljs.highlightAuto(code, ['bash', 'javascript', 'json', 'xml']).value;
    }
});

let renderer = new marked.Renderer();

const pkg = require('./package.json');

const PRODUCTION_MODE = process.env.NODE_ENV === 'production';
const ENVIRONMENT = PRODUCTION_MODE ? 'production' : 'debug';

let filesToBeCopied = [
    {
        context: path.join(__dirname, 'src', 'views', 'favicons', 'slides'),
        from: '*.png',
        toType: 'file',
        to: 'icons/slides'
    }, {
        context: path.join(__dirname, 'src', 'views', 'favicons', 'slides'),
        from: '*.ico',
        toType: 'file',
        to: 'icons/slides'
    }, {
        context: path.join(__dirname, 'node_modules'),
        from: path.join('reveal.js', 'plugin', 'zoom-js'),
        toType: 'dir',
        to: path.join('plugin', 'zoom-js')
    }
];

const IS_DEV_SERVER = (/webpack-dev-server/i).test(process.argv[1]);
if (IS_DEV_SERVER) {
    filesToBeCopied.push({
        context: path.join(__dirname, 'node_modules'),
        from: path.join('reveal.js', 'plugin', 'notes'),
        toType: 'dir',
        to: path.join('plugin', 'notes')
    });
}


let OUTPUT_PATH = '';
//Variales set from cmd line or build scripts. Better use build scripts
let SOCKET_ID = process.env.SOCKET_ID;
let SOCKET_SECRET = process.env.SOCKET_SECRET;
let SOCKET_SERVER_URL = process.env.SOCKET_SERVER_URL;

let generateDocs = false;

const CLIENT_TYPE = typeof process.env.CLIENT_TYPE === 'string' ? process.env.CLIENT_TYPE : (IS_DEV_SERVER ? 'dev-server' : null);
if (CLIENT_TYPE !== null && CLIENT_TYPE !== 'dev-server') {
    //Add multiplex capability
    filesToBeCopied = filesToBeCopied.concat([
        {
            context: path.join(__dirname, 'node_modules'),
            from: path.join('reveal.js', 'plugin', 'multiplex', 'client.js'),
            toType: 'file',
            to: path.join('plugin', 'multiplex', 'client.js')
        }, {
            context: path.join(__dirname, 'node_modules'),
            from: path.join('socket.io-client', 'socket.io.js'),
            toType: 'file',
            to: path.join('socket.io.js')
        }
    ]);

    if (CLIENT_TYPE !== 'master' && CLIENT_TYPE !== 'client') {
        throw `Unsupported CLIENT_TYPE: ${CLIENT_TYPE}`;
    } else {
        console.log(`Building client type ${CLIENT_TYPE}`);
        if (CLIENT_TYPE === 'master') {
            OUTPUT_PATH = 'dist_master';
            filesToBeCopied = filesToBeCopied.concat([
                {
                    context: path.join(__dirname, 'node_modules'),
                    from: path.join('reveal.js', 'plugin', 'multiplex', 'master.js'),
                    toType: 'file',
                    to: path.join('plugin', 'multiplex', 'master.js')
                }, {
                    context: path.join(__dirname, 'node_modules'),
                    from: path.join('reveal.js', 'plugin', 'markdown'),
                    toType: 'dir',
                    to: path.join('plugin', 'markdown')
                }, {
                    context: path.join(__dirname, 'node_modules'),
                    from: path.join('reveal.js', 'plugin', 'notes'),
                    toType: 'dir',
                    to: path.join('plugin', 'notes')
                }]);
        } else {
            OUTPUT_PATH = 'dist_client';
        }
    }
} else {
    console.log('Building standard presentation.');
    OUTPUT_PATH = 'dist';
    generateDocs = true;
}


let definePlugin = new webpack.DefinePlugin({
    TITLE: JSON.stringify(pkg.title),
    VERSION: JSON.stringify(pkg.version),
    PKG_NAME: JSON.stringify(pkg.name),
    DESCRIPTION: JSON.stringify(pkg.description),
    ENVIRONMENT: JSON.stringify(ENVIRONMENT),
    BUILD_DATE: JSON.stringify(new Date()),
    CLIENT_TYPE: JSON.stringify(CLIENT_TYPE),
    SOCKET_ID: JSON.stringify(SOCKET_ID),
    SOCKET_SECRET: JSON.stringify(SOCKET_SECRET),
    SOCKET_SERVER_URL: JSON.stringify(SOCKET_SERVER_URL),
    STATUSBAR_COLOR: JSON.stringify(pkg.config.themeColor),
    API_ENDPOINT: JSON.stringify(ENVIRONMENT === 'production' ? pkg.config.serviceEndpoint : pkg.config.testServiceEndpoint)
});



//Generate index.html for slides
let htmlTemplatePath = path.join(__dirname, 'src', 'views', 'jade', 'html5.jade');
let htmlPlugin = new HtmlWebpackPlugin({
    themeColor: pkg.config.themeColor,
    template: htmlTemplatePath,
    title: pkg.title,
    isTest: typeof process.env.TEST_SLIDES !== 'undefined',
    target: 'slides',
    chunks: ['slides']
});

let entries = {
    slides: path.join(__dirname, 'src', 'index.js')
};

let plugins = [];

if (generateDocs === true) {
    filesToBeCopied = filesToBeCopied.concat([
        {
            context: path.join(__dirname, 'src', 'views', 'favicons', 'docs'),
            from: '*.png',
            toType: 'file',
            to: path.join('icons', 'docs')
        }, {
            context: path.join(__dirname, 'src', 'views', 'favicons', 'docs'),
            from: '*.ico',
            toType: 'file',
            to: path.join('icons', 'docs')
        }
    ]);

    entries['documentation'] = path.join(__dirname, 'src', 'docs.js');
    //Also generate docs.html
    plugins.push(new HtmlWebpackPlugin({
        themeColor: '#002b36',
        template: htmlTemplatePath,
        title: pkg.title + ' Text',
        isTest: typeof process.env.TEST_SLIDES !== 'undefined',
        target: 'docs',
        filename: 'docs.html',
        chunks: ['documentation']
    }));
}

//Copy RevealJS plugins
//Reveal JS Plugins (NOTE: marked and highlight plugin are bypassed using webpack)
let copyRevealPlugins = new CopyWebpackPlugin(filesToBeCopied);

plugins = plugins.concat([copyRevealPlugins, htmlPlugin, definePlugin]);

if (PRODUCTION_MODE) {
    plugins.push(new webpack.optimize.UglifyJsPlugin({
        sourceMap: false
    }));
    plugins.push(new webpack.optimize.OccurenceOrderPlugin());
}

let webpackConfig = {
    context: path.join(__dirname, 'src'),
    entry: entries,
    devServer: {
        // CopyWebpack plugin: This is required for webpack-dev-server. The path should 
        // be an absolute path to your build destination.
        outputPath: path.join(__dirname, 'dist')
    },
    output: {
        //path: USE_CORDOVA ? 'www' : 'dist',
        path: OUTPUT_PATH,
        filename: '[name].js'
    },
    module: {
        loaders: [{
            test: /\.js?$/,
            exclude: /(node_modules)/,
            loader: 'babel',
            query: {
                presets: ['es2015']
            }
        }, {
                test: /\.md$/,
                loader: "html!markdown?gfm=true"
                //loaders: ['html', 'markdown']
            }, {
                test: /\.jade$/,
                loader: 'jade'
            }, {
                test: /\.json$/,
                loader: 'json'
            }, {
                test: /\.scss$/,
                loaders: ['style', 'css', 'postcss', 'sass']
            }, {
                test: /\.css$/,
                loaders: ['style', 'css', 'postcss']
            }, {
                test: /\.woff2?(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=10000&mimetype=application/font-woff"
            }, {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=10000&mimetype=application/octet-stream"
            }, {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: "file"
            },
            // { test: /\.png(\?v=\d+\.\d+\.\d+)?$/, loader: "file" },
            {
                test: /\.ico(\?v=\d+\.\d+\.\d+)?$/,
                loader: "file"
            },
            // { test: /\.jpg(\?v=\d+\.\d+\.\d+)?$/, loader: "file" },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=10000&mimetype=image/svg+xml"
            }, {
                test: /\.jpg(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=250000&mimetype=image/jpeg"
            }, {
                test: /\.png(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=250000&mimetype=image/png"
            }
        ]
    },
    resolve: {
        extensions: ['', '.js', '.jade', '.md', '.json'],
        modulesDirectories: ['node_modules', 'src']
    },
    plugins: plugins,
    postcss: function () {
        return [autoprefixer];
    },
    markdownLoader: {
        //renderer: mdlRenderer
        renderer: renderer
    },
    devtool: 'source-map'
}

module.exports = webpackConfig;