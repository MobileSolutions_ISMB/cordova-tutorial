#!/usr/bin/env node
'use strict';

process.env.CLIENT_TYPE = 'client';
process.env.NODE_ENV = 'production';

const config = require('../multiplex-server.config.json');

let SOCKET_ID = config.socketID;
let SOCKET_SECRET = null;
let SOCKET_SERVER_URL = config.serverURL;

process.env.SOCKET_ID = SOCKET_ID;
process.env.SOCKET_SECRET = SOCKET_SECRET;
process.env.SOCKET_SERVER_URL = SOCKET_SERVER_URL;

let webpack = require("webpack");
let webpackConfig = require('../webpack.config')
// returns a Compiler instance
webpack(webpackConfig, function(err, stats) {
    if(err){
    	console.error("Error",err);
    }
    console.log("Stats",stats);
});