#!/usr/bin/env node

'use strict';

let fs = require('fs');
let path = require('path');

const CONFIG_FILE = 'multiplex-server.config.json';
try {
    fs.accessSync(CONFIG_FILE);
    console.log("Config file already exists... skipping");
} catch (ex) {
    let template = fs.readFileSync(CONFIG_FILE + '.template', 'utf8');
    fs.writeFileSync(CONFIG_FILE, template, 'utf8');
    console.log("Default config file created. Please edit it.");
}