#!/usr/bin/env node
'use strict';

process.env.CLIENT_TYPE = 'master';

const config = require('../multiplex-server.config.json');

let SOCKET_ID = config.socketID;
let SOCKET_SECRET = config.socketSecret;
let SOCKET_SERVER_URL = config.serverURL;

process.env.SOCKET_ID = SOCKET_ID;
process.env.SOCKET_SECRET = SOCKET_SECRET;
process.env.SOCKET_SERVER_URL = SOCKET_SERVER_URL;

process.env.NODE_ENV = 'production';

let webpack = require("webpack");
let webpackConfig = require('../webpack.config')
// returns a Compiler instance
webpack(webpackConfig, function(err, stats) {
    if(err){
    	console.error("Error",err);
    }
    console.log("Stats",stats);
});