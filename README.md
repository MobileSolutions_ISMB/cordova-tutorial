# Cordova Tutorial

Slides for Cordova tutorial

## Slides

All slides are available in HTML format in the **dist** folder.

* the index.html file launches the slides
* the docs.html launches the documentation text (also available in speaker's notes)

The presentation is also available as PDF in pdf/slides.pdf

### Multiplex (a.k.a. Master-Client mode)

If you want to run the slides in client-server mode leveraging [reveal.js multiplex mode](https://github.com/hakimel/reveal.js/#multiplexing),
edit `multiplex-server.config.json` according to your needs, then just follow these instructions.  
Note also that you must rebuild [client](#markdown-header-build-client) and [master](#markdown-header-build-client) 
after modifying the URL in the multiplex-server.config.json file.  
The original behavior was changed because it is a bit confusing and relies on a token generated at the moment,
while here a config based approach was preferred.
This way the master does not even need to be on a local server but can be open from local file system.

#### Launch multiplex server

```
cd multiplex-server
npm install
npm start
```

You can look at the client by browsing at the url configured in `multiplex-server.config.json serverURL`.
Do not forget http:// in the config file.
If this URL is different from localhost, then you can browse slides it from another computer/device.

## Compiling from sources

First of all, run `npm install` after cloning this repository.  

Following are the instructions for generating the slides.

### Simple presentation

`npm run-script build` or `webpack` will create a `dist` folder with a basic client that can either be opened
from file or served statically.

#### Build Client

```
npm run-script build-client
```
will build a static client with websockets connection set according to `multiplex-server.config.json`.
This client will be driven by the master if any.

#### Build Master

```
npm run-script build-master
```
will build a static master client with websockets connection set according to `multiplex-server.config.json`.
This client will be able to drive standard clients, e.g. by turning slides all connected clients will turn slide.

#### Generate PDF

Use [decktape](https://github.com/astefanutti/decktape.git) because RevealJS PDF generation seems to have issues.

For instance (provided you installed node-static):

```bash
cd THIS_REPO
webpack #generate code
static dist #serve
cd PATH_TO_DECKTAPE
./bin/phantomjs decktape.js -s 1280x720 reveal http://localhost:8080 ~/Documents/slides.pdf
```

##### Hint

You may use `node static` for serving the master on another port/computer.

Assuming you installed `node-static` (`npm install -g node-static`) go into the `dist_main` folder and run  
```
static [-a <address>][-p <port>] .
```

You can now open the master presentation at http://address:port/