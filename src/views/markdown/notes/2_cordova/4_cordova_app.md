### Cordova App structure

This picture clearly shows the structure of a Cordova Application.

![Cordova Application](../../../../images/cordovaapparchitecture.png "Structure of a Cordova Application")

Cordova APIs are a set of JavaScript functions, objects and events that help the Web App logic to handle the
WebView API. All standard HTML/DOM APIs (with some minor exceptions depending on the platform) are also
supported. Cordova complements the web view APIs for accessing those OS features that are normally not
available.
Cordova [*plugins*](#cordova-plugins) extend the number of features available to cordova.

##### SPA Design

Designing the web app (logic and UI) as **SPA (Single Page Application)** is the best match for a Cordova
Application.  
In a SPA, all scripts, stylesheets and resources are loaded at start time and routing is done client side.
This design generally has several performance benefits, not just for cordova, and is largely used by modern
web applications.
If any interaction with a web service is needed, it can be made through [XHR](https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest)
, without changing the window URL and thus loading only the data the app will consume.  
You should not be scared about putting al your code in a single script because with modern tools such as
webpack this is a kid game, plus it is possible to optimize script size while keeping track of dependencies,
creating an app bundle.  
SPA design is so popular that is explicitly supported by frameworks such as AngularJS, ReactJS, WinJS and
jQuery Mobile, even if each one with its own peculiarities.
