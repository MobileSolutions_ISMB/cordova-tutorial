### Cordova Plugins

Cordova extends its platforms functionalities by using plugins.
This means that if a feature is not available to the webview for a given platform, it can be added by using a plugin.
Using a plugin system helps the app development to stay modular, using only those features (and their relative user
permissions) that are really needed by the app.  
Furthermore, it also helps debugging and testing each plugin separately, reusing its code in other applications and
keeping the code blocks smaller.

There are plugins for almost every mobile device sensor and API, for instance bluetooth, gps, file system, push
notifications and so on. Not all plugins support all platforms, though. Or if they do, support may be incomplete or
with different features on some platform. This is (should be) generally documented in the plugin documentation.

Cordova development team officially supports just a set of plugins for the most common device features, and they also
provide the templates for the most important target platforms. Cordova "official" plugins were once part of the cordova
platform but they are now been separated. Their names usually start all with "cordova-".

Installing a plugin is quite easy, because plugins are usually available through the
[cordova plugin registry](https://cordova.apache.org/plugins/).
They are also generally available on Github and npm registry. In fact it is possible to install a plugin directly from a
(git) repository, even a private one, using git+https syntax (and the credentials must be entered each time the plugin gets
downloaded because it is not possible to use ssh keys at the moment).
It is also possible to install a plugin directly from a folder, and that's usually the procedure you will use when
*developing/debugging* a plugin.

Each plugin **must** define a `plugin.xml` file which provides information such as:

* plugin name and version
* supported platforms and their required versions
* **app permissions** required by the plugin
* source files to be loaded/compiled/linked
* scripts and assets to be added to the *www* folder
* **third-party dependencies** to be loaded, either libraries/jars/dlls, frameworks or other cordova plugins

It is not uncommon to have a *meta-plugin* which depends on other plugins to work.

The plugin source files are usually splitted in two:

* a JavaScript source folder, usually named `www`
* a native source folder, where there is a folder for each supported platform, each one with platform-specific
sources (written in the platform native languages) or pre-compiled libraries.

The JavaScript interface provides a uniform API that should be called from within the SPA code, and it delegates
to a callback-based mechanism the data-exchange with the bridge native counterpart.  
The callbacks are usually executed through an asynchronous interface named `cordova.exec` and are managed by the
JavaScript plugin code. They accept optional arguments (input), a success callback which can optionally returns plugin
results and an error callback which returns errors.  
Sometimes the native interface is not needed, being the plugin just a wrapper for a remote API, or a meta-plugins that
relies on other cordova plugins to work, or a pure JS implementation of some features, such as a polyfill.

The native interface can do anything other native pieces of code can do: just keep in mind it will usually get executed
in the UI main thread. So if your task does need more time to finish, you should better instantiate new threads here.
It is also possible to load external libraries and frameworks, C++ based pieces of code (where supported) and so on.
Everything has just to be specified in the `plugin.xml` file.

For more details please refer to the [plugin.xml documentation](https://cordova.apache.org/docs/en/latest/plugin_ref/spec.html).
