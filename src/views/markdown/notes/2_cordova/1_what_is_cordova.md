### What is Cordova?
[Cordova](https://cordova.apache.org/) is a popular **mobile** hybrid application development framework:
currently it has reached version 6 and the word *mobile* is no more appropriate, because it also supports
the most popular desktop environments.

Cordova is completely free and open source since Adobe, which bought it from Nitobi, gave it to the
Apache Foundation.    
However Adobe still holds its own Cordova-based tool (more a fork than a tool) with its original name
"[PhoneGap](https://cordova.apache.org/)".

There are several popular frameworks that use cordova as base tool, and several IDEs that support cordova
development. Here are some of the most popular of the aforementioned tools, many of which are at least
partly free and open-source as well:

* [ngCordova](http://ngcordova.com/) combines Cordova and AngularJS
* [Ionic](http://ionicframework.com/) combines Cordova and AngularJS and a native look&feel UI framework
* [Taco](http://taco.tools/) more a Visual Studio Code extension than a framework, supports Cordova and/or
Ionic
* [Intel XKD](https://software.intel.com/en-us/intel-xdk) a complete IDE + graphic tools and framework for
creating x-platform apps + native l&f UI
* [Telerik Platform](http://www.telerik.com/platform#overview) (**warning: not free!**) a complete UI kit +
app framework that supports several technologies and not just cordova
* [Monaca](https://software.intel.com/en-us/intel-xdk") cordova + Onsen framework (combines Angular, React,
jQuery and native l&f for iOS and Android) + various tools and IDE plugins

There may exist many others tools and frameworks out there in the web, including support-plugins for the most
popular IDEs including Visual Studio.

I personally prefer to understand first how the basic tool works, having the highest degree of flexibility,
then try more structured or constrained approaches that use cordova as an engine if needed.
