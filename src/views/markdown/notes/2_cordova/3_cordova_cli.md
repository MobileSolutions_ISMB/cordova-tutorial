### Cordova CLI

The cordova command line interface (CLI) is a tool that can be easily installed using **npm**,
[node.js](https://nodejs.org/) famous package manager.
That's basically all you need to run cordova CLI on your Windows, Linux or Mac computer, since
also node.js is cross-platform.  
You will still need the target platform SDKs installed and available in your PATH variable in
order to do something meaningful with cordova, so please install them if you didn't already.
For this tutorial are required the Android SDK (standalone or with Android Studio bundle) and,
depending on your host platform, Windows 10 UWP SDK (comes with Visual Studio 2015) or iOS SDK
(comes with XCode).

Install cordova by typing:

```bash
npm install -g cordova
```

You should now find the `cordova` command in your shell. If you don't, open a new one.
If you still don't, check your environment variables.

`cordova` util comes with a set of handy options to complete different tasks.  
Type:  

```bash
cordova --help
```

or go to [the official documentation page](https://cordova.apache.org/docs/en/latest/reference/cordova-cli/index.html)
to see the complete list.
