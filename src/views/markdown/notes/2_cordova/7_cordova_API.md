### Cordova API most important features

Once loading cordova.js in our single page application, in order to start using the cordova API we have to listen
to some additional events. In fact, in addition to the `loaded`/`DOMContentLoaded` events exposed by the
browser/webview, which signals that the whole HTML page has been parsed and all scripts have been loaded,
cordova adds some events of its own, which are necessary to handle and make uniform the basic app life cycle events.

#### Core Events

The most important event here is `deviceready`, that signals that cordova API and plugins have been loaded, so from
that moment over the app can start using all those features such as device sensors that are exposed by cordova.
Your app should start working with cordova only after this event, which occurs in a few seconds, and present a loader or
a splash screen in the meanwhile.

Other important events are the `pause` and `resume` events: they are fired from the OS when the app goes to background
and returns to foreground respectively.
The app, as it happens for native apps, should save and then reload the app status here.
The `pause` event is also issued before terminating, or when a child activity such as camera is spawned.

These events are supported by **all** platforms supported by cordova.

There are several other hardware-specific events such as `backbutton`, but they may be available only to a subset
of the supported platforms (e.g. iOS devices have no back button) and you can get a complete support table on the 
cordova documentation page.

All cordova events are fired from the `document` (DOM) object, therefore adding/removing handler is simply like
adding any other listener to `document`.

Example:

```javascript
document.addEventListener('deviceready', deviceReadyHandler);
```

#### The cordova object

A `cordova` JavaScript object is also injected globally and it exposes some properties such as `platformId` and
`platformVersion`, which are specific for each target platform.

It also exposes some methods for handling plugin loading and callback execution, which are more useful for plugin
development.  
Other properties may be appended to the global namespace from other popular plugins such as
[`cordova-plugin-device`](https://github.com/apache/cordova-plugin-device).
