### First cordova app

Creating a cordova **Hello World** application **is terribly easy**.

Creating your first _**meaningful**_ Cordova app _**it is not**_, but requires the same degree of difficulty
as designing and creating any app with other technologies. The tutorial app will provide a slightly more
complicated and real-world like example, analyzing a possible workflow.

In order to create the Hello World app, just follow these instructions:

```bash
mkdir HelloWorld && cd HelloWorld
cordova create .
cordova platform add browser
cordova serve 8000
```

Then go to [http://localhost:8000/browser/www](http://localhost:8000/browser/www) and you should see the
cordova logo with a glowing green message saying "device ready".

Now it is obvious that this app is quite useless, but helps explaining the basic code structure of a cordova app.  
First of all, where is the code created from? The code of the Hello World app is generated from a template that
comes with the `cordova` CLI utility.

If we open the *HelloWorld* folder there are a file, named **config.xml**, and 4 folders, here listed in order
of importance:

* **www/** it's our SPA folder. Here we find the index.html, just like any website/SPA for the web, the resources
such as images, the app stylesheets and the application logic in the *js* folder. This folder is copied
*as it is* into each target platform www folder.
* **platforms/** contains the platforms that we installed with the `platform add` command. `platform list`
will give a list of the available and installed platforms. A platform folder contains all the necessary template
files to create a project for the target platform environment, e.g. Android Studio, XCode, Visual Studio...
and the specific cordova.js script for that platform, which will be loaded at runtime.
* **plugins/** similarly to `platforms/`, this folder contains all the plugins folders for the plugins that we
added with the `plugin add` command. Some plugins, such as *white-list* plugin, are added by default.
* **hooks/** can contain additional scripts that can be executed in various build steps.
For instance I use a hook script for copying some additional Android Lollipop styles.xml files when adding
the android platform.

##### Hello World *www* folder content:  

The index.html file is quite simple: leaving some HTML5 boilerplate, it just contains a couple of divs
and two JavaScript scripts. The first script is `cordova.js`, the second is `js/index.js`.

##### Side note about script positioning

I'll open a parenthesis here, about scripts positioning and loading. In this Hello World app the scripts have been put
at the end of the body, so this means that when the parser will have read the end of index.js, all other body elements
will have been already parsed, and the script cordova.js also loaded (but the ready event has not yet fired!!!).  
You can put the script in the `<head>` tag, as I often prefer to do, but then you have to wait for a DOMContentLoaded event.
Anyway always put the cordova.js as first script because this will reduce the time to wait for the deviceready event.

##### Markup structure

The main div we have here is a container, `div.app`, and a couple of paragraphs. Their styling is all defined in css/index.css,
including the glowing animations and the background. If you're not familiar with CSS, you should, at least a bit.
It's not that bad as it looks.

It is a good practice to use a container such as `div.app` rather to append stuff to the body, and almost any SPA framework will
recommend to do so. This div is the equivalent of our main view in a native app. Each framework could apply its rules for how
the markup should be organized (if any markup is actually needed - see ReactJS), so I will skip this here.

##### App business logic

The `js/index.js` script is quite explicative: it just puts a bunch of methods in an object named app. This is not an object
generated using ES6 classes or ES5 functions, but for simpler tasks as this, it is effective.

The app object is defined then its method `initialize()` gets called. This method calls another methods that binds events.
But what it really does is binding the `onDeviceReady` method to the `deviceready` event.  
Once the `deviceready` event will be fired by cordova, it will call the `receivedEvent` method of app that will actually update
the style attributes of the two `p` elements defined in `index.html`.
Please read the comments in the code -they are quite self explicative- and keep in mind that there are several other ways to do this,
but the intent here is to highlight the importance of the `deviceready` event.

#### The *browser* platform

In order to see this code running you could use the `browser` platform and the *serve* command, that were not mentioned before.

While the `cordova serve` is pretty much similar to how [node-static](https://github.com/cloudhead/node-static#command-line-interface) works,
so it's basically just a static server, the [**browser** platform](https://github.com/apache/cordova-browser) is an implementation of the cordova
API that has been recently included into cordova for debugging purposes only.  
There are plugins that also support the browser platform but they are really few at the moment. It mostly works well only with Google Chrome,
but this allows us to look at how our app is handling cordova events and scripts without having to open an emulator or to launch the app on a device.  
It can be also useful for just testing the UI, but I would rather stick to the webpack dev server with a proper configuration file.
