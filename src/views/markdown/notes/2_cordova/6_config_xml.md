### Cordova config.xml file

The `config.xml` file is the core of a cordova app project.
It stores all the information about a cordova app, which can therefore be serialized and deserialized
on other host machines.
This is useful when collaborating with other developers, or when you need to build your single base
code on different environments such as, for instance, a Mac and a Windows machine when targeting iOS
and Windows Phone respectively, and in general for keeping track of your cordova project settings.

To do so, simply remember to append the `--save` option when performing cordova platform and plugin
add/remove operations.  
If you are using a versioning tool such as git, you may safely exclude the platforms and plugins folders
from the tracking, since they will be generated again by the CLI tool using the `config.xml` file
when running `cordova prepare`.

When using the HelloWorld template, a very basic `config.xml` file is added to the project.
But a complete `config.xml` file will contain much more information, which is used by Cordova, using each
target platform SDK, for generating platform-specific projects using projects templates.  
Each one of these projects can be then open or imported in each platform-specific IDE. This means you can
open and build your cordova-generated code within Android Studio, XCode or Visual Studio if needed.    
Most of the content of `config.xml` will be used for updating each specific app manifest, for instance
with app version number, name, resource paths, permissions and so on.  

When debugging you may just build the app using the CLI (depending on the platform
it could be more or less comfortable) but in several situations opening the apps within the IDE allows
for fine-tuning some parameters, signing with developer keys and compiling for release in a more comfortable
way.

Keep in mind, however, that since the *www* folder is **_copied_** to the destination platform folder,
you should run the `prepare` command each time you update the code in the www folder. If you run the
`build` or `run` commands, the `prepare` is also executed.
If it happens to you to modify the code in the generated project folder, for instance from within the IDE
editor, don't forget to report your changes back to the *www* folder code. Since you're working on a copy,
you wouldn't loose all the changes you made once generating the project again.

A complete reference for the `config.xml` file can be found
[in the official documentation page](https://cordova.apache.org/docs/en/latest/config_ref/index.html).
