### Supported platforms
Cordova supports several platforms, some of which are not mobile. However the main focus of this tutorial is
on mobile platforms, where the support is surely large.

#### Mobile platforms
Android and iOS are the best supported platforms (also in terms plugin availability) and Windows is gaining
pretty good support as well, also because of its native JavaScript API support since version 8.1.
Other supported platforms include Blackberry, FireOS (Amazon) and webOS.

#### Desktop platforms
Support for desktop platforms has a kind of lower priority and some plugins could be missing. However, since
Windows 10 UWP (and a bit since 8.1) offers the same API for phone, desktop and iOT, deploying an app that
supports both Windows Desktop and Phone is more or less the same, if your app is properly designed.
The app market for Windows UWP has also been unified.  
Deploying to MacOS X desktop and Ubuntu Linux may have distribution issues with their respective markets, as
far as I know.

#### Older platform versions
Legacy platforms such as Android ICS to KitKat, Windows 8, iOS <=8 etc. are also supported, but keep in mind
that the larger the set of devices and platforms to be included, the smaller the number of advanced features
that can be included seamlessly. There exists some polyfills and tools that help supporting older platform
versions such as [Crosswalk](https://crosswalk-project.org/), but I would personally advice not to support very
old platforms, unless strictly mandatory, if performances are a concern. After all, this is true also for
native apps.

#### Unified API
How does cordova support so many platforms, each one "speaking" a different native language?
The trick is that each of these platforms exposes a **_WebView_**, that is the "magical" container
that was mentioned before, where scripts get executed. Cordova simply implements and exposes
a so called *bridge* between the WebView JavaScript API and the native platform API, making its
interface as uniform as possible for all supported platforms.
Specific features, which may be shared only by a subset of platforms/devices (think about NFC, for
instance) are available through plugins.
