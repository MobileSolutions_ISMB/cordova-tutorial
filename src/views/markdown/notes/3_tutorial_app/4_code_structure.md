### JavaScript Code Explanation

In Cordova Apps, most of the logic is written in JavaScript: specific tasks can be written
in native languages (plugins code), but the business logic is usually written in JavaScript
(or languages that can be transpiled to JS such as CoffeeScript or TypeScript).

In "Tiny Tag Scanner" the code is written in JS ES6, which gets transpiled using BabelJS in order
to avoid ES6 compatibility issues.  
The architecture of this app is really simple, but the workflow described here can be used
also for more complex apps that fully implement design patterns such as MVC or MVVM.
The choice will also depend on the frameworks adopted for the implementation.  

The main entry point is `src/main.js`. This module only requires all dependencies
(both internal and third-party), and then waits for the deviceready event.  
Using webpack it is possible to take advantage of the dev server for implementing and debugging
the UI and the cordova-independent business logic components without having to wait for the
device/emulator to deploy each time a change to the code is made.   
However since the dev-server does not support cordova, for cordova-dependent components it is necessary
to follow an alternate flow. A possible way of doing this is defining some constants in the `webpack.config.js`
file. These constants can be also passed, for instance, to the *webpack html plugin* (the one generating the HTML
code starting from a template) and to the app at build time, using the *webpack define plugin*.
Alternatively, since `cordova` will always be an object in the global namespace (i.e. appended to `window`)
it is possible to check for cordova at runtime:

```javascript
if (typeof cordova === 'undefined') {
    //do stuff without cordova - e.g. when using webpack dev server
    document.addEventListener('DOMContentLoaded', onStart);
}
else {
    //cordova is ready to go
    document.addEventListener('deviceready', onStart);
}
```

In "Tiny Tag Scanner", the `onStart` method checks if NFC is available for current device and
modifies the CSS class list of the main app container element accordingly.
This way if no NFC is available, the stylesheet will hide the NFC-related messages in the markup,
leaving only those related to the qr code.
Then the `onStart` method will create the `app` controller and the `dialog` controller as
instances of the `Application` and `InfoPopup` ES6 classes respectively.

The `dialog` object is just a simple controller for a modal dialog, where some information about
our app is displayed when clicking on the top-right corner button, using some constants defined with
the webpack define plugin and some taken from the cordova platform.

The `app` object, on the other hand, contains all the business logic. The basic task of this app is
reading information from NFC tags or qr-codes, then parsing their contents, distinguishing between URLs
and plain text, and finally display them in a list with a different look depending on their type and
source (the way they are represented is handled by the list item view template).

The `Application` class is therefore the one responsible of handling the I/O events fired by the NFC and qr code
plugins, and then displaying the results to the user. This class is also responsible of binding the user
input from the main view (the only view in this app) and trigger appropriate events.  

##### NFC listener

NFC messages (NDEF) is a complex NFC standard format that defines how messages are read by NFC tag readers,
including those embedded in most mobile phones. Since it needs to store a lot of information in the few space
available on each tag, it squeezes bytes in order to save space. Luckily the NFC plugin used in this app
provides some handy helpers that are useful for parsing NDEF messages. The `ndefHandler` function uses these helpers
for parsing NDEF messages when tapping on a tag. It is registered as listener for the NFC plugin NDEF event
with the following statement:

```javascript
nfc.addNdefListener(ndefHandler);
```

The `nfc` and `ndef` objects are the global objects provided by [the NFC plugin](https://github.com/chariotsolutions/phonegap-nfc).

##### QR-code listener

The [qr code plugin](https://github.com/phonegap/phonegap-plugin-barcodescanner) uses a different
pattern for data exchange, because the app has to trigger the scan explicitly, in this case on user input
when clickin the purple button on the UI. This plugin has to be called passing a success and a failure callback:

```javascript
cordova.plugins.barcodeScanner.scan(onScanSuccess, onScanError);
```

##### Controlling the view

Either if it is read from NFC or from qr code, once we have a text line, this is added to a list view using
the `Application::addItemToList` method. This method will add a new item to the list and hide the initial instruction
message (by setting its height to 0, so that a nice yet very simple CSS animation is performed).  
Its complementary method `Application::clearList` will remove all items and restore the initial message height.
