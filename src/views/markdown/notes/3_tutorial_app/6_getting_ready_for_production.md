### Last steps before production

When prototyping and debugging, it is useful to have unminified JavaScript code. Furthermore, webpack
generates source maps, that are recognized by the browser developer tools and help to understand
which module is generating an exception or where are breakpoints set, even when using ES6+Babel.

#### Debugging

When debugging the UI it is possible to use `webpack-dev-server`, which features live reloading. This
is useful for debugging the cordova independent components of our apps, just like any normal website,
but unfortunately this does not support cordova API yet.

An intermediate, but optional step, is using the [cordova browser platform](https://github.com/apache/cordova-browser),
which is basically meant for debugging. Several, but not all plugins, also provide a browser platform support,
emulating real devices inputs and outputs where possible. This can be useful when debugging cordova events.

I added the browser platform to the tutorial app, however keep in mind that things on real devices
often behave in a very different way.

**Browser developer tools** such as Chrome Developer Tools or Safari Inspector are needed for debugging
both mobile websites and cordova apps. Application compiled in debug mode are visible just like any mobile browser
tabs on a connected device.

* For android devices type `chrome://inspect` in a Google Chrome tab on your PC for inspecting connected web apps.
* For ios devices it is necessary to enable developer options on Mac OS Safari and then search the inspectable target in the menu
* For windows devices it is necessary to use the Visual Studio inspector.

Plugin/native code exceptions can be debugged using the target platform IDEs, setting breakpoints into native code.

##### Release

Once debugging is over, the JavaScript code can be bundled so that it occupies less space in the
app package and gets loaded faster by the webview.

Moreover, compiling the app with release flags ensures that nobody will look into the source code because it
will become no more inspectable. Release build also strips debug symbols out, increasing overall performances.

We can compile the app in release mode either with

```bash
cordova build <PLATFORM> --release
```

or by opening the IDE and setting appropriated build options.

Opening the IDE gives usually more control to this process with respect to cordova CLI for this step,
especially when signing the app with a developer certificate before submitting it to the app stores.
However setting proper hooks and CLI options can make even this step fully automated.

In order to submit the app to the stores it is necessary to have purchased a valid developer license for each
target platform, and having set up all the certificates in the same way as for native apps.
