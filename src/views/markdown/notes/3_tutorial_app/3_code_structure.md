### Code structure

All the source code can be found into the `src` folder, while the *bundle generated* code will
go into the *www* folder. That's why the *www* folder was also added to the .gitignore file.

The code in the `src` folder can be written using modules, easier to read and to debug, leaving
to webpack the burden of generating the app bundle, optionally including some code optimization
when making the production release.

There are several web app frameworks around the web that could be (should be) chosen for a new app,
such as ReactJS, AngularJS, WinJS, backbone, whatever...
Since this is just a tutorial and the app is not really complex, the code uses only plain ES6
"vanilla" js for the business logic and [Google MDL](https://getmdl.io/index.html) as graphical framework
for the views.  
This choice was made because most of us are familiar with the Material Design, plus the framework is quite
clean and unobtrusive, playing well also on non-google devices. Aesthetic decisions should be taken from
qualified personnel only, but in the end they're matter of taste.
In this tutorial some basic [SASS](http://sass-lang.com/) styling was added for some little customizations.
You may of course still stick to old plain CSS if you feel more comfortable with, but using a tool like
SASS or LESS is a great boost.

Finally, since I'm kinda lazy at writing HTML, I used the [Jade/Pug](http://jade-lang.com/)
templating engine for the markup, the same you would generally use in NodeJS+Express web apps.
Jade is a non-verbose, simple and extensible 100% HTML5 compliant framework.
There are several other template engines out there, or frameworks such as ReactJS that do not
really require a template engine at all. You can always stick to old plain HTML in your apps if
you feel more comfortable.

One way or another, using template engines is possible only thanks to the
[HtmlWebpackPlugin](https://github.com/ampedandwired/html-webpack-plugin) which saves us the
time needed for generating all the markup, also adding our bundled app scripts to it.

#### Folder structure

This is the source folder structure of "Tiny Tag Scanner".

```
webpack.config.js
src/
    contentSecurityPolicy.txt
    main.js
    js/
        Application.js
        InfoPopup.js
    style/
        stylesheet.scss
    views/
        html5.jade
        index.jade
        infoPopup.jade
        listItem.jade
```

For those familiar with C++, `webpack.config.js` can be considered a Makefile equivalent, while
the generated output goes into the *www* folder, where cordova will read it from.  

The contentSecurityPolicy.txt is a text file where we put all the CSP required by the
[cordova-whitelist-plugin](https://github.com/apache/cordova-plugin-whitelist) to avoid
[XSS](https://en.wikipedia.org/wiki/Cross-site_scripting) security issues.  
See [CSP Reference](http://content-security-policy.com/) for more details. Due to its verbosity,
I decided to move it to a separate text file.

The stylesheet will load MDL fonts and base styles, and also perform some customizations for the theme
colors, and some minor adaptation for list elements and dialog elements.
