### Creating a Cordova App

This tutorial shows a possible workflow for creating a Cordova App
that runs across multiple platforms performing the same tasks.

This is just *one* possible workflow as there may be other way to do this.
This tutorial will show you *the way of the command line*, because understanding how
plain old CLI works will help you get out of troubles (if any) whenever
any fancy UI tool gets stuck.

The tutorial shows how to create a simple app using a NFC plugin and a QR Code scanning plugin,
helping you to understand how easy it could be accessing device capabilities that are not available
within the browser. The app name is *"Tiny Tag Scanner*.

For the sake of simplicity, this tutorial app will be compatible just with the most
popular platforms, namely Android, Windows (+Phone) >= 8.1 and iOS.

#### Requirements

You will need to install:

* node.js (>=5.x) -> then install [`cordova`](https://cordova.apache.org/#getstarted) *and*
[`webpack`](https://webpack.github.io/docs/installation.html) using `-g` option
* git
* on any machine: Android SDK (make sure `android` and `adb` are in your PATH)
* on Windows 10 machines: Windows 10 SDK (comes with VS 2015)
* on Mac OS machines: XCode + command line tools
* a (possibly lightweight) text editor: I recommend Visual Studio Code, but it's your choice
