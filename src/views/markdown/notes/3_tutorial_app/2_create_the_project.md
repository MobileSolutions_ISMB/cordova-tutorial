### Create the project

This is how to create a new cordova project in a folder using cordova CLI.

The `create` command will create a new project: a `config.xml` file, initializing
it with the parameters passed from the CLI. Same thing as per the HelloWorld app,
but this time more parameters will be passed.

For a list of the possible parameters type `cordova help create` (use `help for
general help, and help + command name for specific help).

```bash
mkdir tiny-tag-scanner
cordova create tiny-tag-scanner it.ismb.TinyTagScanner Tiny\ Tag\ Scanner
cd tiny-tag-scanner
```

Looking at the folder content (`ls`) you will see that all the basic stuff has
been put there by Cordova.

Since this is a completely new project, remove the content of the *www* folder.
It is wort noting that with cordova 6.0 you can use **templates**.
This means that you can re-use an existing app skeleton as a template (pretty much like the
HelloWorld template included with cordova) even from another git repository.  
[This post](http://moduscreate.com/new-in-cordova-6-app-templates-using-git/) gives
good hints on how using templates.  
You can of course re-use an app of your own if you wish!

```bash
rm -rf www/*
```

The `config.xml` file in the tutorial repository was edited for the tutorial purposes,
so you can see the differences with the basic one at this stage.

#### Add platforms and plugins

Type these commands in order to add the platforms supported, that is android, ios, windows and *browser*
(for debugging purposes). Using the `--save` option will ensure that these are added to
the config.xml file.

```bash
cordova platform add android --save
cordova platform add ios --save
cordova platform add windows --save
cordova platform add browser --save
```

Plugins are added in a very similar way:

```bash
cordova plugin add cordova-plugin-device --save
cordova plugin add cordova-plugin-statusbar --save
cordova plugin add cordova-plugin-splashscreen --save
cordova plugin add phonegap-plugin-barcodescanner --save
cordova plugin add phonegap-nfc --save
```

I added some commonly used official plugins plus a plugin for qr codes and one for NFC.

Each platform template, distinguished by the `<engine>` tag in the config file, has its own version.
This specifies which platforms are we generating the project for.  
Plugins have a similar `<plugin>` tag.
It is possible to add platforms and plugins specifying their version number with *@number*
notation. It is also possible to add a platform from a git repository.  
Other important tags are `<preference>` tags, which specify settings consumed by some plugins
or platforms. They can be in the global space or under some `<platform>` tag, where all
platform-specific options are, and then they will be valid only for that specific platform.  
Please read carefully the documentation of the plugins you are including in your projects!

#### Creating package.json

In order to track the app front-end dependencies and versions, a `package.json` has been added.
This is useful when sharing our project with the others, or when building on different machines,
not to mention *maintaining* the code over time.

```bash
npm init
```

Answer the interactive shell questions in a proper way, then hit confirm. If you don't know what
to answer, just leave empty. You can fill the all the fields lately. You could only get some
warnings here and there when using npm. [This site](http://package-json-validator.com/), along
with [package.json specifics](https://docs.npmjs.com/files/package.json), helps filling the
required fields, most of which will be automatically be filled by `npm`.   
At the end of this procedure, a basic `package.json` file will be available in our folder, where
all the front-end and development dependencies will be added over time.

The `package.json` file has several fields, some of which are optional, but are worth mentioning:

* the **scripts** field contains a set of script that can be executed automatically,
e.g. before or after `npm install`, and custom scripts that can be executed by the `npm run-script` command
* the **config** field can hold about any kind of data. You can use this field to put configuration
options or variables.

In this tutorial the basic `package.json` was also already prepared, so you can see the differences
with the basic one.

#### Initing a git repository

This is an optional step that I strongly advice to follow when creating a new project.  
Initializing a new repository is dead simple:

```bash
git init
```

In this tutorial I provided a `.gitignore` file that can be used as a template for several projects
like this, with minimum adjustments.
You can create a `.gitignore` file on your own ignoring dependency folders such as
`node_modules` and generated folders such as `platforms` and `plugins`, keeping your
repository code essential, as well as other editor- or file explorer- generated file.

#### Using webpack

As shown in previous tutorials, we are going to use [webpack](https://webpack.github.io/)
to build our app. This helps a lot keeping the code structure as modular as possible,
leveraging ES6 and a lot of its helpful features, along with webpacks bundling capabilities.

```bash
npm install webpack --save-dev
npm install webpack-dev-server --save-dev
```

This tutorial also provides a `webpack.config.js` file that can be used as a template. I tried to
put as many comments as possible to make understandable what it does.

#### README.md

Last, but not least, **always drop a few lines in a README.md** file in your repository root.
This will help other developers (or your future self) to understand what's this repository about.
