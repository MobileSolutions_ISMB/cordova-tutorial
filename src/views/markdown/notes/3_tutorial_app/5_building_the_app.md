### Building the app

As mentioned in previous sections, a webpack config file can be used like a C++ Makefile: here you can
add targets, set variables, define constants and so on.

This is very useful when debugging, or when managing different configurations for our project, for instance
a "test release" with different service endpoints or service calls than the production release.

The webpack configuration of "Tiny Tag Scanner" creates a bundle of our application from the *src* folder
and puts it in the *www* folder. For modern, complex, JavaScript application this step is almost unavoidable
and also helps keeping track of external and internal dependencies.

Just like for the `HelloWorld` project, here the `cordova prepare` command will read all files from the *www* folder,
execute **hook scripts**, and then copy assets and plugin configurations according to the `config.xml`
file.

Running `cordova prepare` also downloads all the platform and plugins stored in `config.xml` with the
specified versions when it's launched for the first time.

#### Using npm scripts

Although not required, defining scripts that can be executed via npm is very helpful. They can be used, for instance,
for setting environment variables used by webpack and other tasks, speeding up the build process while reducing
the probability of making manual mistakes.

For these app the build scripts are located in the *script* folder and are referenced in the `package.json`
*scripts* section.

```json
"scripts": {
    "build-debug-app": "node ./build_scripts/compile_debug_app.js",
    "build-production-app": "node ./build_scripts/compile_production_app.js",
    "dev-server": "webpack-dev-server --hot --inline --progress --colors --history-api-fallback",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
```

They can be run by typing:

```bash
npm run-script <SCRIPT NAME>
```

More details at [`package.json` reference](https://docs.npmjs.com/misc/scripts).

In details, the following script:

```bash
npm run-script build-debug-app
```

will bundle the JavaScript code in debug mode, with unminified code, suitable for debugging, and then also call
`cordova prepare` while

```bash
npm run-script build-production-app
```

will do the same in production mode, telling webpack to minify the JavaScript and CSS code in the bundle, reducing
its size.

Webpack offers a lot of features for app bundling through its several plugins and loaders, including the possibility of
generating separated chunks libraries that can be optionally loaded at runtime, but this falls outside this tutorial scope.

#### Platform-specific quirks and tweaks

Cordova tries make the app development as uniform as possible on all platforms, but all the platforms are not equal.
Some platform may require additional configuration, often because their bridge or some of the plugins are not up to
date with the current IDEs.

For instance, the nfc plugin used in this tutorial does not set the `Proximity` capability in the Windows 10
project manifest (it does, however, when compiling for Windows 8.1).  
Furthermore, the qr code plugin uses a native runtime component which is not compatible with the default
`any cpu` Windows 10 target, so it requires to specify `arm` build target for Windows Phones and `x86` or `x64` for
PCs and tablets (this is written in the plugin README).

In this case these issues can be easily be fixed by opening Visual Studio and editing the projects settings properly.

In general, when using plugins, it is better to read carefully their documentation for searching
platform-specific configurations or quirks, and sometimes also to read the issue list if problems occur.
