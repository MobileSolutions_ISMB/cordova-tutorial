### Terminology

It is better to introduce some terminology, in order to better understand the differences
between regular webpage and mobile applications.

#### Web App
A **web app** is a remote application whose UI runs in a browser,
while the business logic is on a remote machine.
Most of the time, a web app requires user login and cannot access local data except for that exposed through the browser sandbox.
Popular web apps are social networks such as Facebook,
or productivity tools like Google Documents or Microsoft Office Online.
The line between a web app and a simple webpage is quite thin (a web app is
indeed a webpage, but not vice-versa).  
Often we refer to a web page to something that has less interaction and
exposes less services than a web app.
Updating a web app most of the time is as fast as updating a webpage, at least
for the front-end part.  
This depends a lot on the web app design, where it is deployed (e.g. on cloud),
which third-party services consumes and so on. Things may get really complex, so
I will not dive into this arguments here.

#### Mobile App
A **mobile app** is a software that runs on a mobile platform and it is
usually distributed through app stores.
We most often think of a mobile app as something simpler than desktop
apps we are all used to since Windows 95, but most of the time the complexity
is just the same if not greater.  
Mobile apps can access local data (sandboxed by the mobile OS and user permissions)
and device sensors.
Releasing or updating a mobile app requires submitting the app package to
the platform app store, often undergoing a review process and so on.
It takes from days to weeks for your app (or app update) to be available for everyone
on the market.

#### Mobile Web App
A **mobile web app** is a web app that is optimized for (recent) mobile devices and
their browser, which have become as advanced as their desktop counterparts but have
additional features such as touch gestures and device sensors.
Most of the times a mobile web app runs quite well on both mobile and desktop,
just presenting different UIs, which adapt to different screen sizes and features.
This is called *responsive design* but I will not discuss about it here, just remember
it is nowadays essential when creating a new website.
Mobile web apps can be saved on the mobile launcher screen and run full screen
with a user experience very close to real mobile apps.
Making a website or web app suitable for mobile is just a matter of adding a few meta
tags and icons to the HTML head.
Full screen mobile apps can even lock the browser orientation and they can leverage full
HTML5 API, WebGL, and access mobile sensors such as camera, accelerometer or GPS.
A typical example are mobile games web apps.
Anyway, without an internet connection none or little interaction is possible.
Updating a mobile web app is just the same as updating a web app.

#### Mobile Hybrid App
A **mobile hybrid app** is a mobile app whose UI runs in a Web View.
This is the magical *hybrid* component that most mobile platform offer
to the developers, which becomes a *bridge* between device API and web API.
Very often the UI components are HTML/JS based and are shown in a Web View,
while those device API that are usually not available to the browser
(e.g. Push Notifications, NFC, SD Card...)
are accessed using a *bridge* component written in the native language.  
An hybrid app **does not need** internet access to run.
Releasing or updating is similar to a mobile app.
However sometimes it is even possible to update only the web-based UI from remote
without undergoing the full update release cycle (so called hot update).
