### When to choose hybrid apps

There could be several reasons to choose hybrid apps development instead of other way of distributing
or developing our app, such as native apps or mobile web apps or using other cross-development tools.
The evaluation must be done, as always, carefully, weighing all design needs, case by case.

For instance, if we are happy with our mobile web app but we feel that distributing it through mobile
platform app stores can help broadening its usage, we may think about wrapping it as an hybrid app
(often named Hosted Web App).
There are even tool that automate this such as [manifold.js](http://manifoldjs.com/) or
[Windows UWP bridge for hosted web apps](https://microsoftedge.github.io/WebAppsDocs/en-US/win10/CreateHWA.htm).

Another reason to go for hybrid apps is when the app needs also to work offline. Mobile web app can
partially work offline once cached, by storing data in the localStorage, but there could be limitations
such as storage available.  
These limitations are not a problem if we develop an hybrid app, because it can access system API such as
file system (the real one, not the HTML5 sandboxed one) or Bluetooth, NFC and so on.

There are also evaluations in terms of staff skills and effort: for instance cross-platform apps could help
when there are not many developers, much effort or enough budget for maintaining 3 different codebases for
our project. Web-based development is often the cheapest cross-platform developing technique and will obviously
fit better if our developers team has some/good HTML/JS skills.

Finally, HTML-based techniques often fit better for rapid prototyping, as there are several well-tested UI
frameworks, some of which also have visual tools, thus offering several different ways of designing the UI.  
This applies well either if we want to offer a native-like user experience, as there are several frameworks
that try to imitate native UIs, or if we want to offer a highly customized user experience.

HTML front-end development techniques fall outside the scope of this tutorial, but for sure they offer 
great flexibility, especially on recent devices and browser that consistently reduced the performance gap
with respect to the native UI frameworks.
