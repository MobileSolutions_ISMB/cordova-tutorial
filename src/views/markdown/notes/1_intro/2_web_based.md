### Web-based development

Web-based development is based on HTML/JS/CSS, which is quite easy to learn
(but not to master) compared to its native counterparts.
It also requires a simpler setup, because it is script-based and does not need
compiling. Minimal requirements are just a text editor and a web browser.  
Debugging a web page is also dead-simple by using the browser inspection tools,
which nowadays are quite advanced with respect to the past.  
Deploying is quite simple: it's often just a matter of dragging a folder or a file
in a static server directory.  
Finally, the web developers community is huge, perhaps the largest one, and so is
the amount of libraries and modules freely available online, for instance on npm,
bower or github ([npm is right now the largest module repository](http://www.modulecounts.com/)).
