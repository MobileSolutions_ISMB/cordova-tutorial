### Motivations

Everybody would like his app to be available for the largest audience, 
however there are only 2-3 mobile platforms which are worth developing for,
while the others share no significant market shares or are obsolete.  
Nevertheless, having to maintain N different code bases for N target platforms is 
very expensive, so adopting cross-platform development techinques can be a relief.  
In several cases (but not always!) it is possible to adopt tools where a 
single code base can be deployed, with minimal adjustment, to different platforms.

There are several techiniques for achieving cross-platform development, mainly:

* using cross-compilers such as CMake for C++
* using a programming language which is mapped and compiled to each target platform 
native language (e.g. like Xamarin, React Native), exposing a subset of the native features
* using a common container where scripts are executed

The subject of this tutorial is web-based cross-platform development, which falls into the
latter category.