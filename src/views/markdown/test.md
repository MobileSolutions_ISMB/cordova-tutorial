## Cordova project

An [Apache Cordova](https://cordova.apache.org/) project

```javascript
    function onReady(){
        //Device and plugins loded!
        //Now you can do stuff with the cordova device and plugins API
    }

    document.addEventListener('ready', onReady);
```