require('!!file-loader?name=icons/docs/[name].[ext]&context=./src!views/favicons/docs/manifest.json');
require('style/docsstyle.scss');
require('highlight.js/styles/tomorrow-night-bright.css');

import 'material-design-lite/material'; //Import MD (componentHandler) in the global namespace

function init() {
    window.location.hash = '';
    //Create index
    let sections = [].slice.call(document.querySelectorAll('section'));
    let drawerNav = document.querySelector('nav.mdl-navigation');
    let tabBar = document.querySelector('div.mdl-layout__tab-bar');

    sections.forEach(function (s) {
        let heads = [].slice.call(s.querySelectorAll('h3'));
        let navSection = drawerNav.querySelector(`a.mdl-navigation__link[href="#${s.id}"]`);
        heads.forEach((h) => {
            let a = document.createElement('a');
            a.href = `#${h.id}`;
            a.textContent = h.textContent;
            a.className = 'mdl-navigation__link small';
            navSection.appendChild(a);
        });
    });

    function onDrawerNavClick(e) {
        setTimeout(() => {
            let d = document.querySelector('.mdl-layout');
            d.MaterialLayout.toggleDrawer();
        }, 100);
    }

    function onTabBarClick(e) {
        let activeTab = tabBar.querySelector('a.is-active');
        let href = activeTab.href.split('#')[1];
        let oldNavBarActiveSection = drawerNav.querySelector('a.is-active');
        if (oldNavBarActiveSection) {
            oldNavBarActiveSection.classList.remove('is-active');
        }
        let newNavBarActiveSection = drawerNav.querySelector(`a.mdl-navigation__link[href="#${href}"]`);
        if (newNavBarActiveSection) {
            newNavBarActiveSection.classList.add('is-active');
        }
        let activeSection = document.querySelector('section.is-active');
        if(activeSection && activeSection.scrollIntoView){
            activeSection.scrollIntoView();
        }
        window.location.hash= '';
    }

    drawerNav.addEventListener('click', onDrawerNavClick, false);
    tabBar.addEventListener('click', onTabBarClick, false);
}

//Fired when all scripts and styles have been loaded
document.addEventListener('DOMContentLoaded', init, false);