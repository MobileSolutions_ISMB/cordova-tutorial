//Raw copy of the file
require('!!file-loader?name=icons/slides/[name].[ext]&context=./src!views/favicons/slides/manifest.json');
require('style/style.scss');
require('highlight.js/styles/monokai-sublime.css');
require('presentable/src/presentable.css');
//require('reveal.js/css/theme/moon.css');

require('reveal.js/lib/js/head.min');
import Reveal from 'reveal.js/js/reveal';
import presentable from 'presentable/src/controller.js';

//Handler that gets called when Reveal is ready to go
function onReady(event) {
    // event.currentSlide, event.indexh, event.indexv
    let overlay = document.querySelector('div.loading-overlay');
    overlay.addEventListener('transitionend', () => {
        overlay.remove();
    });
    overlay.style.opacity = 0;
    overlay.style.zIndex = 0;

    document.querySelector('div.reveal').style.opacity = 1;
}

//Handler that gets called on each slide changed
function onSlideChanged(event) {
//    console.debug("Slide changed event", event);
    // event.previousSlide, event.currentSlide, event.indexh, event.indexv
    let dataset = event.currentSlide.dataset;
    if (dataset && typeof dataset['backgroundIframe'] !== 'undefined' /* add others*/ ) {
        document.querySelector('div.slides').classList.add('with-overlay');
    } else {
        document.querySelector('div.slides').classList.remove('with-overlay');
    }
}

//Handler that initializes the slides
function init() {
   // console.debug("PARAMS: client=", CLIENT_TYPE, "socketSecret=", SOCKET_SECRET, "socket ID=", SOCKET_ID, "socket server url=", SOCKET_SERVER_URL)
    Reveal.addEventListener('ready', onReady);

    Reveal.addEventListener('slidechanged', onSlideChanged);

    let dependencies = [{
        src: 'plugin/zoom-js/zoom.js',
        async: true
    }];

    let multiplex = null;

    switch (CLIENT_TYPE) {
        case 'master':
            dependencies = dependencies.concat([{
                src: 'plugin/notes/notes.js',
                async: true
            }, {
                src: 'socket.io.js',
                async: true
            }, {
                src: 'plugin/multiplex/master.js',
                async: true
            }, {
                src: 'plugin/multiplex/client.js',
                async: true
            }]);
            multiplex = {
                secret: SOCKET_SECRET,
                id: SOCKET_ID,
                url: SOCKET_SERVER_URL
            };
            break;
        case 'client':
            dependencies = dependencies.concat([{
                src: 'socket.io.js',
                async: true
            }, {
                src: 'plugin/multiplex/client.js',
                async: true
            }]);
            multiplex = {
                secret: null,
                id: SOCKET_ID,
                url: SOCKET_SERVER_URL
            };
            break;
        case 'dev-server':
            dependencies = dependencies.concat([{
                src: 'plugin/notes/notes.js',
                async: true
            }]);
            break;
        case null:
        default:
            break
    }

    Reveal.initialize({
        multiplex: multiplex,
        dependencies: dependencies
    });
    Reveal.configure({
        slideNumber: 'c/t',
        parallaxBackgroundImage: require('images/ISMB_Contour.png'),
        parallaxBackgroundSize: '1595px 100vh', //'1595px 1015px',
        parallaxBackgroundHorizontal: null,
        parallaxBackgroundVertical: 0,
        transition: 'convex'
    });
    presentable.toc({
        framework: "revealjs",
        hideNoTitle: true
    });
    window.Reveal = Reveal;
}

//Fired when all scripts and styles have been loaded
document.addEventListener('DOMContentLoaded', init, false);